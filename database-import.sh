#!/usr/bin/env bash

DB=$1
FILE=$2

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. You need to add the database name and sql file. For instance: database-import.sh scld-prtl ~/Downloads/file.sql"
    exit 1
fi

QUERY="DROP DATABASE IF EXISTS $1; CREATE DATABASE $1; USE $1; set unique_checks=0; set foreign_key_checks=0; set autocommit=0; source $2; set unique_checks=1; set foreign_key_checks=1; set autocommit=1"
echo $QUERY

mysql -e "$QUERY"
