#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. You need to add the solr version. For instance: startSolr.sh 6"
    exit 1
fi

VERSION=$1
DETACHED="DETACHED STATE"
DETACHEDFLAG=-d
SOLRFOLDER=/Users/fabianderijk/Docker/solr

if [ $# -eq 2 ]
  then
    DETACHEDFLAG=''
    DETACHED="NO DETACHED STATE"
fi

echo "Stopping al solr instances"
docker stop $(docker ps -a -q --filter="name=solr" --format="{{.ID}}")

echo "Starting composer based SOLR $VERSION - $DETACHED"
cd $SOLRFOLDER$VERSION
docker-compose up $DETACHEDFLAG
