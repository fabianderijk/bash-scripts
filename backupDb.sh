#!/usr/bin/env bash

# Instantiate variables
DRUSH=`which drush`

CONNECT=`$DRUSH sql-connect`
SITENAME=`$DRUSH cget system.site name`

IFS=':' read -ra PARTS <<< "$SITENAME"
SITENAME=${PARTS[$(expr ${#PARTS[@]} - 1)]}
SITENAME="${SITENAME//\'}"
SITENAME="${SITENAME// }"

$DRUSH sql-dump --gzip --result-file="~/Downloads/$SITENAME-$(date +%Y-%m-%d-%H.%M.%S).sql"

