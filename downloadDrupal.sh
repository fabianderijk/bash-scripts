#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. You need to add the folder name For instance: downloadDrupal.sh site-folder."
    exit 1
fi

# Instantiate variables
COMPOSER=`which composer`
ADDVHOST=`which addVhost.sh`
FOLDERNAME=$1

cd /Users/fabianderijk/Sites

$COMPOSER create-project drupal-composer/drupal-project:8.x-dev $FOLDERNAME --stability dev --no-interaction --verbose
cd $FOLDERNAME/web
valet link $FOLDERNAME

while true; do
    read -p "Do you wish to download some modules we always use? y|n " yn
    case $yn in
        [Yy]* ) cd ../; $COMPOSER require drupal/paragraphs drupal/fortytwo drupal/pathauto drupal/admin_toolbar drupal/config_split drupal/token drupal/config_ignore; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
