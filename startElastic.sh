#!/usr/bin/env bash

DETACHED="DETACHED STATE"
DETACHEDFLAG=-d
SOLRFOLDER=/Users/fabianderijk/Docker/elasticsearch

if [ $# -eq 2 ]
  then
    DETACHEDFLAG=''
    DETACHED="NO DETACHED STATE"
fi

echo "Stopping al elastic instances"
docker stop $(docker ps -a -q --filter="name=elasticsearch" --format="{{.ID}}")

echo "Starting composer based ElasticSearch $VERSION - $DETACHED"
cd $SOLRFOLDER
docker-compose up $DETACHEDFLAG
