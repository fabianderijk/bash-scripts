#!/usr/bin/env bash

echo "Stopping al solr instances"
docker stop $(docker ps -a -q --filter="name=solr" --format="{{.ID}}")