#!/usr/bin/env bash

DRUSH='/Applications/MAMP/bin/php/php7.1.1/bin/php -d memory_limit=1024M /Users/fabianderijk/drush/drush.php';

$DRUSH sql-query "UPDATE users SET name = 'admin' WHERE uid = 1";
$DRUSH upwd admin --password=admin;

$DRUSH -y en devel dblog views_ui field_ui;

$DRUSH updb -y;
$DRUSH fra -y;
$DRUSH cc all;
$DRUSH fra -y;
$DRUSH cc all;
$DRUSH fra -y;
$DRUSH cc all;
