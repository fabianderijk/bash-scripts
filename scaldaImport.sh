cd /Users/fabianderijk/Sites/scalda-portals-cms/web
../vendor/drush/drush/drush mrs group_cluster
../vendor/drush/drush/drush mrs group_location
../vendor/drush/drush/drush mrs group_cluster_subgroup
../vendor/drush/drush/drush mrs group_location_subgroup
../vendor/drush/drush/drush mrs group_service
../vendor/drush/drush/drush mrs group_department
../vendor/drush/drush/drush mrs group_department_subgroup
../vendor/drush/drush/drush mrs group_service_subgroup
../vendor/drush/drush/drush mrs group_team
../vendor/drush/drush/drush mrs group_study
../vendor/drush/drush/drush mrs group_student_group
../vendor/drush/drush/drush mrs group_student_group_subgroup
../vendor/drush/drush/drush mrs group_study_subgroup
../vendor/drush/drush/drush mrs group_team_subgroup
../vendor/drush/drush/drush mrs user_employee
../vendor/drush/drush/drush mi --update --all
../vendor/drush/drush/drush sapi-r
../vendor/drush/drush/drush sapi-i