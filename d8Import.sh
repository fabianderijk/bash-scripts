#!/usr/bin/env bash

# Instantiate variables
CURRENTFOLDER=`pwd`
COMPOSER=`which composer`
GIT=`which git`
DRUSH=$CURRENTFOLDER'/vendor/drush/drush/drush';

if [ ! -f composer.json ]; then
    echo "Composer file not found. Are you in the root directory of a Drupal 8 install?"
    exit 0;
fi

$GIT pull --rebase
$COMPOSER install --profile -n -v

cd web

$DRUSH entup -y;
$DRUSH updb -y;
$DRUSH cr;

if [ $1 = "excluded" ]
  then
    echo "Use the excluded config split"
    $DRUSH csex excluded -y;
fi

$DRUSH cim -y;
$DRUSH csim -y;
$DRUSH cr;
