#!/usr/bin/env bash

echo "Stopping al elasticsearch instances"
docker stop $(docker ps -a -q --filter="name=elasticsearch" --format="{{.ID}}")