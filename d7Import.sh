#!/usr/bin/env bash

# Instantiate variables
CURRENTFOLDER=`pwd`
GIT=`which git`
DRUSH=`which drush`

$GIT pull --rebase
$GIT submodule init
$GIT submodule update

$DRUSH updb -y
$DRUSH fra -y
$DRUSH cc all
$DRUSH fra -y
$DRUSH cc all
$DRUSH fra -y
$DRUSH l10n-update-refresh
$DRUSH l10n-update
$DRUSH cc all